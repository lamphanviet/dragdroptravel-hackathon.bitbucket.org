/**
 * Created by lamphanviet on 1/13/15.
 */
app.controller('dragDropController', ['$scope', 'dragDropService', 'regionService', 'tripService', function ($scope, dragDropService, regionService, tripService) {
    $scope.msg = "dragDropController";

    $scope.trips = tripService.trips;

    $scope.selectedTrip = null;

    $scope.setSelectedTrip = function(trip) {
        $scope.selectedTrip = trip;
        displaySelectedTripOnMap();
        console.log("Selected trip: " + trip.id);
    };

    var map = dragDropService.getMap(),
        mapOptions = dragDropService.getMapOptions();
    var currentLocation = null,
        currentLocationMarker = null;
    var tripMarkers = [];

    (function initialize() {
        mapInitialize();
        dragDropInitialize();
    })();

    function mapInitialize() {
        console.log("initialize map");
        map = new google.maps.Map(document.getElementById('map-canvas'),
            mapOptions);

        // Get user current location
        // Try HTML5 geolocation
        if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                currentLocation = new google.maps.LatLng(position.coords.latitude,
                    position.coords.longitude);

                currentLocationMarker = new google.maps.Marker({
                    map: map,
                    position: currentLocation,
                    title: 'Your location',
                    icon: {
                        url: "images/pins/currentLocation.png",
                        scaledSize: new google.maps.Size(32, 32)
                    }
                });
                map.setCenter(currentLocation);
            }, function() {
                handleNoGeolocation(true);
            });
        } else {
            // Browser doesn't support Geolocation
            handleNoGeolocation(false);
        }

        //addRandomMarker();
    }

    $scope.createTrip = function() {
        $scope.setSelectedTrip(tripService.createTrip());
    }

    function dragDropInitialize() {
        tripService.getRandomTrip();
        tripService.getRandomTrip();
        tripService.getRandomTrip();
        $scope.setSelectedTrip($scope.trips[0]);
    }

    function displaySelectedTripOnMap() {
        function addTripMarker(name, lat, lng) {
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                map: map,
                draggable: true,
                title: name,
                icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: 20,
                    strokeWeight: 2,
                    fillOpacity: 1,
                    fillColor: '#FF0000',
                    strokeColor: '#FF0000',
                    radius: 10000000
                }
            });
            attachMarkerName(marker, name);
            tripMarkers.push(marker);
        }

        if ($scope.selectedTrip == null) return;

        for (var i = 0; i < tripMarkers.length; i++)
            tripMarkers[i].setMap(null);

        var trip = $scope.selectedTrip;
        addTripMarker(trip.fromLocation.name, trip.fromLocation.latLong.lat, trip.fromLocation.latLong.lng);
        for (var i = 0; i < trip.destinations.length; i++) {
            var region = trip.destinations[i].location;
            addTripMarker(region.name, region.latLong.lat, region.latLong.lng);
        }
    }

    function attachMarkerName(marker, name) {
        var infowindow = new google.maps.InfoWindow({
            content: name
        });
        infowindow.open(marker.get('map'), marker);
    }

    function addRandomMarker() {
        var myLatlng = new google.maps.LatLng(-25.363882,131.044922);
        // Place a draggable marker on the map
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            draggable:true,
            title:"London",
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 10,
                strokeWeight: 2,
                fillOpacity: 0.5,
                fillColor: '#FF0000',
                strokeColor: '#FF0000',
                radius: 1000000000000
            }
        });
    }
}]);