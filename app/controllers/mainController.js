/**
 * Created by lamphanviet on 1/13/15.
 */
app.controller('mainController', ['$scope', '$location', function($scope, $location) {

    $scope.isActive = function(route) {
        return route == $location.path();
    }
}]);