/**
 * Created by lamphanviet on 1/13/15.
 */
var app = angular.module('app',[
    'ngRoute'
]);

app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/something', {
            templateUrl: 'app/views/something.html'
        })
        .when('/about', {
            templateUrl: 'app/views/about.html'
        })
        .when('/dragdrop', {
            templateUrl: 'app/views/dragDrop.html',
            controller: 'dragDropController'
        })
        .when('/careers', {
            templateUrl: 'app/views/careers.html'
        })
        .otherwise({
            redirectTo: '/dragdrop'
        });
}]);
// testing changes