/**
 * Created by lamphanviet on 1/13/15.
 */
var DataJSON = new function() {
    var regions = [
        {
            "center": {
                "title": "Adelaide, SA, Australia",
                "lng": 138.59996,
                "lat": -34.92861
            },
            "timezone": "Australia/Adelaide",
            "area": [
                -35.348,
                138.435,
                -34.562,
                139.047
            ],
            "name": "AU_SA_Adelaide"
        },
        {
            "center": {
                "title": "Amsterdam, Netherlands",
                "subtitle": "+ The Hague, Rotterdam, Utrecht",
                "lng": 4.89793,
                "lat": 52.37115
            },
            "timezone": "Europe/Amsterdam",
            "area": [
                51.82,
                3.92,
                53,
                5.28
            ],
            "name": "NL_W_Randstad"
        },
        {
            "center": {
                "title": "Atlanta, GA, USA",
                "lng": -84.38884,
                "lat": 33.75563
            },
            "timezone": "US/Eastern",
            "area": [
                33.2869,
                -84.8968,
                34.3434,
                -83.801
            ],
            "name": "US_GA_Atlanta"
        },
        {
            "center": {
                "title": "Auckland, New Zealand",
                "lng": 174.76771,
                "lat": -36.84448
            },
            "timezone": "Pacific/Auckland",
            "area": [
                -37.5,
                174.4,
                -36.5,
                175.2
            ],
            "name": "NZ_N_Auckland"
        },
        {
            "center": {
                "title": "Austin, TX, USA",
                "subtitle": "+ San Antonio",
                "lng": -97.741,
                "lat": 30.263
            },
            "timezone": "America/Chicago",
            "area": [
                28.95,
                -99,
                30.85,
                -97.1
            ],
            "name": "US_TX_Austin"
        },
        {
            "center": {
                "title": "Bahia Blanca, Argentina",
                "lng": -62.2686,
                "lat": -38.7194
            },
            "timezone": "America/Argentina/Buenos_Aires",
            "area": [
                -38.947,
                -62.506,
                -38.629,
                -61.955
            ],
            "name": "AR_B_BahiaBlanca"
        },
        {
            "center": {
                "title": "Berlin, Germany",
                "lng": 13.41035,
                "lat": 52.52115
            },
            "timezone": "Europe/Berlin",
            "area": [
                52.15,
                12.51,
                52.89,
                14.18
            ],
            "name": "DE_BE_Berlin"
        },
        {
            "center": {
                "title": "Bordeaux, France",
                "lng": -0.58,
                "lat": 44.839
            },
            "timezone": "Europe/Paris",
            "area": [
                44.2531,
                -1.4227,
                45.7484,
                0.39
            ],
            "name": "FR_B_Bordeaux"
        },
        {
            "center": {
                "title": "Boston, MA, USA",
                "lng": -71.059,
                "lat": 42.361
            },
            "timezone": "America/New_York",
            "area": [
                42.15,
                -71.4,
                42.6,
                -70.7
            ],
            "name": "US_MA_Boston"
        },
        {
            "center": {
                "title": "Brisbane, QLD, Australia",
                "subtitle": "+ Gold Coast, Sunshine Coast",
                "lng": 153.0175,
                "lat": -27.4651
            },
            "timezone": "Australia/Brisbane",
            "area": [
                -28.5,
                150.1,
                -22.8,
                153.7
            ],
            "name": "AU_QLD_Southern"
        },
        {
            "center": {
                "title": "Budapest, Hungary",
                "lng": 19.047,
                "lat": 47.502
            },
            "timezone": "Europe/Budapest",
            "area": [
                47.06,
                18.73,
                47.75,
                19.43
            ],
            "name": "HU_Budapest"
        },
        {
            "center": {
                "title": "Cairns, QLD, Australia",
                "subtitle": "+ Townsville, Mackay",
                "lng": 145.771,
                "lat": -16.9204
            },
            "timezone": "Australia/Brisbane",
            "area": [
                -21.7,
                145.4,
                -16.5,
                149.5
            ],
            "name": "AU_QLD_Northern"
        },
        {
            "center": {
                "title": "Calgary, AB, Canada",
                "lng": -114.0581,
                "lat": 51.0458
            },
            "timezone": "America/Edmonton",
            "area": [
                50.6,
                -114.5,
                51.3,
                -113.65
            ],
            "name": "CA_AB_Calgary"
        },
        {
            "center": {
                "title": "Canberra, ACT, Australia",
                "lng": 149.12863,
                "lat": -35.28161
            },
            "timezone": "Australia/Sydney",
            "area": [
                -35.975,
                148.6739,
                -35.0944,
                149.5075
            ],
            "name": "AU_ACT_Canberra"
        },
        {
            "center": {
                "title": "Chicago, IL, USA",
                "lng": -87.6298,
                "lat": 41.87811
            },
            "timezone": "America/Chicago",
            "area": [
                41.3,
                -88.7,
                42.7,
                -87.5
            ],
            "name": "US_IL_Chicago"
        },
        {
            "center": {
                "title": "Christchurch, New Zealand",
                "subtitle": "+ Timaru",
                "lng": 172.637,
                "lat": -43.531
            },
            "timezone": "NZ",
            "area": [
                -44.7,
                170.9,
                -43,
                173
            ],
            "name": "NZ_S_Christchurch"
        },
        {
            "center": {
                "title": "Cincinnati, OH, USA",
                "lng": -84.5114,
                "lat": 39.1048
            },
            "timezone": "America/New_York",
            "area": [
                38.829,
                -84.8735,
                39.3906,
                -84.2253
            ],
            "name": "US_OH_Cincinnati"
        },
        {
            "center": {
                "title": "Cleveland, OH, USA",
                "lng": -81.69516,
                "lat": 41.50132
            },
            "timezone": "America/New_York",
            "area": [
                41.2024,
                -82.1379,
                41.6796,
                -81.384
            ],
            "name": "US_OH_Cleveland"
        },
        {
            "center": {
                "title": "Dallas, TX, USA",
                "subtitle": "+ Fort Worth",
                "lng": -96.8,
                "lat": 32.782
            },
            "timezone": "America/Chicago",
            "area": [
                32.3,
                -97.75,
                33.35,
                -96.25
            ],
            "name": "US_TX_Dallas"
        },
        {
            "center": {
                "title": "Darwin, NT, Australia",
                "lng": 130.8391,
                "lat": -12.4542
            },
            "timezone": "Australia/Darwin",
            "area": [
                -12.9,
                130.5,
                -12.1,
                131.4
            ],
            "name": "AU_NT_Darwin"
        },
        {
            "center": {
                "title": "Denver, CO, USA",
                "subtitle": "+ Boulder",
                "lng": -104.994,
                "lat": 39.745
            },
            "timezone": "America/Denver",
            "area": [
                39.45,
                -105.59,
                40.23,
                -104.66
            ],
            "name": "US_CO_Denver"
        },
        {
            "center": {
                "title": "Detroit, MI, USA",
                "lng": -83.0507,
                "lat": 42.33693
            },
            "timezone": "US/Eastern",
            "area": [
                42.2331,
                -83.3608,
                42.4787,
                -82.8788
            ],
            "name": "US_MI_Detroit"
        },
        {
            "center": {
                "title": "Dublin, Ireland",
                "lng": -6.25954,
                "lat": 53.34845
            },
            "timezone": "Europe/Dublin",
            "area": [
                52.56,
                -7.81,
                53.95,
                -5.7
            ],
            "name": "IE_Dublin"
        },
        {
            "center": {
                "title": "Edmonton, AB, Canada",
                "lng": -113.49,
                "lat": 53.558
            },
            "timezone": "America/Edmonton",
            "area": [
                53.2,
                -114,
                53.8,
                -113.1
            ],
            "name": "CA_AB_Edmonton"
        },
        {
            "center": {
                "title": "Halifax, NS, Canada",
                "lng": -63.573,
                "lat": 44.654
            },
            "timezone": "America/Halifax",
            "area": [
                44.4,
                -63.9,
                45,
                -63.2
            ],
            "name": "CA_NS_Halifax"
        },
        {
            "center": {
                "title": "Helsinki, Finland",
                "lng": 24.941,
                "lat": 60.173
            },
            "timezone": "Europe/Helsinki",
            "area": [
                59.96,
                24.27,
                60.65,
                25.74
            ],
            "name": "FI_Helsinki"
        },
        {
            "center": {
                "title": "Ho Chi Minh City, Vietnam",
                "lng": 106.69853,
                "lat": 10.77084
            },
            "timezone": "Asia/Ho_Chi_Minh",
            "area": [
                10.36,
                106.32,
                11.19,
                107.02
            ],
            "name": "VN_SG_HoChiMinhCity"
        },
        {
            "center": {
                "title": "Hobart, TAS, Australia",
                "subtitle": "+ Launceston, Burnie",
                "lng": 147.32887,
                "lat": -42.88571
            },
            "timezone": "Australia/Sydney",
            "area": [
                -43.7,
                144.55,
                -40.65,
                148.6
            ],
            "name": "AU_TAS"
        },
        {
            "center": {
                "title": "Honolulu, HI, USA",
                "lng": -157.85568,
                "lat": 21.30455
            },
            "timezone": "US/Hawaii",
            "area": [
                21.1332,
                -158.3212,
                21.7978,
                -157.4423
            ],
            "name": "US_HI_Honolulu"
        },
        {
            "center": {
                "title": "Houston, TX, USA",
                "lng": -95.368,
                "lat": 29.766
            },
            "timezone": "America/Chicago",
            "area": [
                29.25,
                -96.05,
                30.3,
                -94.7
            ],
            "name": "US_TX_Houston"
        },
        {
            "center": {
                "title": "Las Vegas, NV, USA",
                "lng": -115.1361,
                "lat": 36.16727
            },
            "timezone": "America/Los_Angeles",
            "area": [
                35.9352,
                -115.3715,
                36.3328,
                -114.779
            ],
            "name": "US_NV_LasVegas"
        },
        {
            "center": {
                "title": "London, UK",
                "lng": -0.118,
                "lat": 51.519
            },
            "timezone": "Europe/London",
            "area": [
                51.237,
                -0.562,
                51.745,
                0.282
            ],
            "name": "UK_London"
        },
        {
            "center": {
                "title": "Los Angeles, CA, USA",
                "lng": -118.242,
                "lat": 34.078
            },
            "timezone": "America/Los_Angeles",
            "area": [
                33.35,
                -118.95,
                34.35,
                -116.8
            ],
            "name": "US_CA_LosAngeles"
        },
        {
            "center": {
                "title": "Louisville, KY, USA",
                "lng": -85.73394,
                "lat": 38.20143
            },
            "timezone": "America/Kentucky/Louisville",
            "area": [
                37.9355,
                -86.0106,
                38.4418,
                -85.3239
            ],
            "name": "US_KY_Louisville"
        },
        {
            "center": {
                "title": "Madrid, Spain",
                "lng": -3.707,
                "lat": 40.415
            },
            "timezone": "Europe/Madrid",
            "area": [
                40.25,
                -3.91,
                40.57,
                -3.51
            ],
            "name": "ES_MD_Madrid"
        },
        {
            "center": {
                "title": "Manchester, UK",
                "lng": -2.241,
                "lat": 53.48
            },
            "timezone": "GB",
            "area": [
                53.369,
                -2.6641,
                53.6535,
                -1.9638
            ],
            "name": "UK_GM_Manchester"
        },
        {
            "center": {
                "title": "Melbourne, VIC, Australia",
                "subtitle": "+ Geelong",
                "lng": 144.97183,
                "lat": -37.81137
            },
            "timezone": "Australia/Melbourne",
            "area": [
                -39.2,
                141,
                -34,
                148.3
            ],
            "name": "AU_VIC_Melbourne"
        },
        {
            "center": {
                "title": "Mexico City, Mexico",
                "lng": -99.1276,
                "lat": 19.4284
            },
            "timezone": "America/Mexico_City",
            "area": [
                19.247,
                -99.4324,
                19.6523,
                -98.774
            ],
            "name": "MX_DIF_MexicoCity"
        },
        {
            "center": {
                "title": "Miami, FL, USA",
                "subtitle": "+ Fort Lauderdale, Key Largo",
                "lng": -80.225,
                "lat": 25.795
            },
            "timezone": "US/Eastern",
            "area": [
                24.7,
                -81.1,
                26.4,
                -80
            ],
            "name": "US_FL_Miami"
        },
        {
            "center": {
                "title": "Milan, Italy",
                "lng": 9.187,
                "lat": 45.465
            },
            "timezone": "Europe/Rome",
            "area": [
                45.33,
                9.02,
                45.61,
                9.37
            ],
            "name": "IT_Milano"
        },
        {
            "center": {
                "title": "Minneapolis, MN, USA",
                "subtitle": "+ St. Paul",
                "lng": -93.2746,
                "lat": 44.977
            },
            "timezone": "America/Chicago",
            "area": [
                44.4,
                -94.4,
                45.8,
                -92.55
            ],
            "name": "US_MN_Minneapolis"
        },
        {
            "center": {
                "title": "Montreal, QC, Canada",
                "lng": -73.541,
                "lat": 45.529
            },
            "timezone": "America/Montreal",
            "area": [
                45,
                -74.4,
                46.7,
                -72.9
            ],
            "name": "CA_QC_Montreal"
        },
        {
            "center": {
                "title": "Nantes, France",
                "lng": -1.55451,
                "lat": 47.21852
            },
            "timezone": "Europe/Paris",
            "area": [
                47.1108,
                -1.7853,
                47.3472,
                -1.3252
            ],
            "name": "FR_R_Nantes"
        },
        {
            "center": {
                "title": "New York City, NY, USA",
                "lng": -74.00614,
                "lat": 40.71669
            },
            "timezone": "America/New_York",
            "area": [
                40.49,
                -74.27,
                40.93,
                -73.2
            ],
            "name": "US_NY_NewYorkCity"
        },
        {
            "center": {
                "title": "Orlando, FL, USA",
                "subtitle": "+ Cocoa Beach, Cape Canaveral, Melbourne",
                "lng": -81.37905,
                "lat": 28.54212
            },
            "timezone": "US/Eastern",
            "area": [
                27.8488,
                -81.7822,
                28.8832,
                -80.4996
            ],
            "name": "US_FL_Orlando"
        },
        {
            "center": {
                "title": "Oslo, Norway",
                "lng": 10.79249,
                "lat": 59.9233
            },
            "timezone": "Europe/Oslo",
            "area": [
                59.4,
                9.98,
                60.92,
                12.14
            ],
            "name": "NO_Oslo"
        },
        {
            "center": {
                "title": "Ottawa, ON, Canada",
                "lng": -75.6968,
                "lat": 45.4234
            },
            "timezone": "America/Montreal",
            "area": [
                44.9,
                -76.3,
                45.8,
                -75.1
            ],
            "name": "CA_ON_Ottawa"
        },
        {
            "center": {
                "title": "Paris, France",
                "lng": 2.33686,
                "lat": 48.8545
            },
            "timezone": "Europe/Paris",
            "area": [
                48.68,
                2.08,
                49.07,
                2.6
            ],
            "name": "FR_J_Paris"
        },
        {
            "center": {
                "title": "Perth, WA, Australia",
                "lng": 115.85747,
                "lat": -31.95299
            },
            "timezone": "Australia/Perth",
            "area": [
                -35.5,
                114,
                -28,
                122
            ],
            "name": "AU_WA_Perth"
        },
        {
            "center": {
                "title": "Philadelphia, PA, USA",
                "lng": -75.164,
                "lat": 39.953
            },
            "timezone": "America/New_York",
            "area": [
                39.61,
                -75.67,
                40.32,
                -74.6
            ],
            "name": "US_PA_Philadelphia"
        },
        {
            "center": {
                "title": "Phoenix, AZ, USA",
                "lng": -112.08928,
                "lat": 33.45106
            },
            "timezone": "America/Phoenix",
            "area": [
                33.0133,
                -112.6593,
                33.7792,
                -111.5744
            ],
            "name": "US_AZ_Phoenix"
        },
        {
            "center": {
                "title": "Pittsburgh, PA, USA",
                "lng": -80.00042,
                "lat": 40.44073
            },
            "timezone": "America/New_York",
            "area": [
                40.2449,
                -80.2881,
                40.6963,
                -79.6413
            ],
            "name": "US_PA_Pittsburgh"
        },
        {
            "center": {
                "title": "Portland, OR, USA",
                "lng": -122.676,
                "lat": 45.525
            },
            "timezone": "US/Pacific",
            "area": [
                44.25,
                -124.1,
                46.25,
                -122.1
            ],
            "name": "US_OR_Portland"
        },
        {
            "center": {
                "title": "Porto Alegre, Brazil",
                "lng": -51.21725,
                "lat": -30.02932
            },
            "timezone": "America/Sao_Paulo",
            "area": [
                -30.2629,
                -51.343,
                -29.8707,
                -50.9509
            ],
            "name": "BR_PortoAlegre"
        },
        {
            "center": {
                "title": "Quebec City, QC, Canada",
                "lng": -71.241,
                "lat": 46.814
            },
            "timezone": "Canada/Eastern",
            "area": [
                46.7,
                -71.6,
                47,
                -71
            ],
            "name": "CA_QC_QuebecCity"
        },
        {
            "center": {
                "title": "Rennes, France",
                "lng": -1.679,
                "lat": 48.116
            },
            "timezone": "Europe/Paris",
            "area": [
                47.86,
                -2,
                48.37,
                -1.34
            ],
            "name": "FR_E_Rennes"
        },
        {
            "center": {
                "title": "Rome, Italy",
                "lng": 12.481,
                "lat": 41.877
            },
            "timezone": "Europe/Rome",
            "area": [
                41.17,
                11.65,
                42.22,
                13.16
            ],
            "name": "IT_Rome"
        },
        {
            "center": {
                "title": "Sacramento, CA, USA",
                "lng": -121.498,
                "lat": 38.57797
            },
            "timezone": "America/Los_Angeles",
            "area": [
                38.2035,
                -122.1611,
                39.2982,
                -120.91
            ],
            "name": "US_CA_Sacramento"
        },
        {
            "center": {
                "title": "Saint Louis, MO, USA",
                "lng": -90.19565,
                "lat": 38.62934
            },
            "timezone": "US/Central",
            "area": [
                38.1842,
                -90.997,
                39.0192,
                -89.5358
            ],
            "name": "US_MO_SaintLouis"
        },
        {
            "center": {
                "title": "Salt Lake City, UT, USA",
                "lng": -112.07068,
                "lat": 40.72559
            },
            "timezone": "US/Mountain",
            "area": [
                39.8296,
                -112.5769,
                41.7057,
                -111.2476
            ],
            "name": "US_UT_SaltLakeCity"
        },
        {
            "center": {
                "title": "San Diego, CA, USA",
                "lng": -117.16377,
                "lat": 32.71463
            },
            "timezone": "US/Pacific",
            "area": [
                32.5051,
                -117.7295,
                33.483,
                -116.1227
            ],
            "name": "US_CA_SanDiego"
        },
        {
            "center": {
                "title": "San Francisco, CA, USA",
                "subtitle": "+ San Jose, Oakland",
                "lng": -122.41379,
                "lat": 37.77952
            },
            "timezone": "America/Los_Angeles",
            "area": [
                36.85,
                -123.1,
                38.2,
                -121.5
            ],
            "name": "US_CA_SanFrancisco"
        },
        {
            "center": {
                "title": "Santa Rosa, CA, USA",
                "lng": -122.71443,
                "lat": 38.44047
            },
            "timezone": "America/Los_Angeles",
            "area": [
                38.2185,
                -123.1155,
                38.8322,
                -122.4419
            ],
            "name": "US_CA_SantaRosa"
        },
        {
            "center": {
                "title": "Santiago, Chile",
                "lng": -70.66269,
                "lat": -33.44863
            },
            "timezone": "America/Santiago",
            "area": [
                -33.574,
                -70.7993,
                -33.342,
                -70.4958
            ],
            "name": "CL_SA_Santiago"
        },
        {
            "center": {
                "title": "Seattle, WA, USA",
                "lng": -122.327,
                "lat": 47.616
            },
            "timezone": "US/Pacific",
            "area": [
                47.1,
                -122.6,
                48,
                -121.7
            ],
            "name": "US_WA_Seattle"
        },
        {
            "center": {
                "title": "Stavanger, Norway",
                "lng": 5.73138,
                "lat": 58.96633
            },
            "timezone": "Europe/Oslo",
            "area": [
                58.0197,
                4.5374,
                60.0484,
                7.3279
            ],
            "name": "NO_Rogaland"
        },
        {
            "center": {
                "title": "Strasbourg, France",
                "lng": 7.747,
                "lat": 48.583
            },
            "timezone": "Europe/Paris",
            "area": [
                48.46,
                7.58,
                48.71,
                7.89
            ],
            "name": "FR_A_Strasbourg"
        },
        {
            "center": {
                "title": "Sydney, NSW, Australia",
                "subtitle": "+ Newcastle, Wollongong",
                "lng": 151.20699,
                "lat": -33.86749
            },
            "timezone": "Australia/Sydney",
            "area": [
                -35.25,
                149.5,
                -32,
                152.67
            ],
            "name": "AU_NSW_Sydney"
        },
        {
            "center": {
                "title": "Tampa, FL, USA",
                "subtitle": "+ Bradenton, Sarasota, St. Petersburg",
                "lng": -82.458,
                "lat": 27.965
            },
            "timezone": "US/Eastern",
            "area": [
                26.9,
                -82.9,
                28.3,
                -82.1
            ],
            "name": "US_FL_Tampa"
        },
        {
            "center": {
                "title": "Thunder Bay, ON, Canada",
                "lng": -89.249,
                "lat": 48.392
            },
            "timezone": "America/Thunder_Bay",
            "area": [
                48.29,
                -89.64,
                48.6,
                -88.95
            ],
            "name": "CA_ON_ThunderBay"
        },
        {
            "center": {
                "title": "Toronto, ON, Canada",
                "lng": -79.379,
                "lat": 43.667
            },
            "timezone": "America/Toronto",
            "area": [
                43.1,
                -80.7,
                44.4,
                -79
            ],
            "name": "CA_ON_Toronto"
        },
        {
            "center": {
                "title": "Toulouse, France",
                "lng": 1.442,
                "lat": 43.603
            },
            "timezone": "Europe/Paris",
            "area": [
                43.41,
                1.2,
                43.8,
                1.68
            ],
            "name": "FR_N_Toulouse"
        },
        {
            "center": {
                "title": "Trento, Italy",
                "lng": 11.1196,
                "lat": 46.0706
            },
            "timezone": "Europe/Rome",
            "area": [
                45.68,
                10.34,
                46.49,
                11.79
            ],
            "name": "IT_Trentino"
        },
        {
            "center": {
                "title": "Turin, Italy",
                "lng": 7.689,
                "lat": 45.072
            },
            "timezone": "Europe/Rome",
            "area": [
                44.36,
                6.76,
                45.76,
                8.54
            ],
            "name": "IT_Torino"
        },
        {
            "center": {
                "title": "Vancouver, BC, Canada",
                "lng": -123.1139,
                "lat": 49.26123
            },
            "timezone": "America/Vancouver",
            "area": [
                49,
                -123.7,
                50.3,
                -121.3
            ],
            "name": "CA_BC_Vancouver"
        },
        {
            "center": {
                "title": "Victoria, BC, Canada",
                "lng": -123.364,
                "lat": 48.427
            },
            "timezone": "America/Vancouver",
            "area": [
                48.25,
                -124.08,
                48.73,
                -123.24
            ],
            "name": "CA_BC_Victoria"
        },
        {
            "center": {
                "title": "Washington, DC, USA",
                "subtitle": "+ Baltimore, MD",
                "lng": -77.0365,
                "lat": 38.8976
            },
            "timezone": "America/New_York",
            "area": [
                38.49,
                -77.63,
                39.54,
                -76.37
            ],
            "name": "US_DC_Washington"
        },
        {
            "center": {
                "title": "Wellington, New Zealand",
                "lng": 174.7791,
                "lat": -41.27845
            },
            "timezone": "NZ",
            "area": [
                -41.4407,
                174.5151,
                -40.7223,
                175.6192
            ],
            "name": "NZ_N_Wellington"
        },
        {
            "center": {
                "title": "Winnipeg, MB, Canada",
                "lng": -97.13,
                "lat": 49.916
            },
            "timezone": "America/Winnipeg",
            "area": [
                49.7,
                -97.4,
                50,
                -96.9
            ],
            "name": "CA_MB_Winnipeg"
        }
        ];

    return {
        regions: regions
    }
}