/**
 * Created by lamphanviet on 1/13/15.
 */
function Region(data) {
    this.id = guid();
    this.name = data ? data.center.title : "";
    this.code = data ? data.name : "";
    this.timezone = data ? data.timezone : "";
    this.latLong = new LatLong(data ? data.center.lat : 0, data ? data.center.lng : 0);
    this.area = data.area;
    this.population = data.population;
}

Region.prototype.contain = function(latLong) {
    return this.area[0] <= latLong.lat && latLong.lat <= this.area[2]
        && this.area[1] <= latLong.lng && latLong.lng <= this.area[3];
}

Region.prototype.clone = function() {
    var res = new Region();
    res.id = this.id;
    res.name = this.name;
    res.code = this.code;
    res.timezone = this.timezone;
    res.latLong = new LatLong(this.latLong.lat, this.latLong.lng);
    res.area = this.area;
    res.population = this.population;
    return res;
}