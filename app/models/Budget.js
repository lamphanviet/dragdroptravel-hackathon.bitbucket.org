/**
 * Created by lamphanviet on 1/13/15.
 */
var BUDGET_TYPE = new function() {
    var BUDGET = { id: 0, value: 3, name: "budget", code: "budget" },
        ECONOMY = { id: 1, value: 5, name: "economy", code: "economy" },
        COMFORT = { id: 2, value: 7, name: "comfort", code: "comfort" },
        LUXURY = { id: 3, value: 10, name: "luxury", code: "luxury" };
    var budgetList = [BUDGET, ECONOMY, COMFORT, LUXURY];

    function getRandomBudget() {
        var n = Math.floor((Math.random() * 100) + 1) % 4;
        return budgetList[n];
    }

    return {
        BUDGET: BUDGET,
        ECONOMY: ECONOMY,
        COMFORT: COMFORT,
        LUXURY: LUXURY,
        getRandomBudget: getRandomBudget
    }
};