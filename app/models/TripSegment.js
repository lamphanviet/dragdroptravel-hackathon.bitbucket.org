/**
 * Created by lamphanviet on 1/13/15.
 */
function TripSegment(data) {
    this.type = data.type;
    this.fromLocation = data.fromLocation;
    this.toLocation = data.toLocation;
    this.duration = data.duration;
    this.startDate = data.startDate;
    this.price = data.price;
    this.ticket = data.ticket;
}

TripSegment.prototype.getName = function() {
    var res = "";
    res += this.type.name;
    if (this.type == TRIP_SEGMENT_TYPE.FLIGHT)
        res += " " + this.fromLocation.name + " to " + this.toLocation.name;
    res += " $" + this.price;
    return res;
}

var TRIP_SEGMENT_TYPE = new function() {
    var FLIGHT = { id: 0, value: 1, name: "flight" },
        HOTEL = { id: 1, value: 1, name: "hotel" };
    return {
        FLIGHT: FLIGHT,
        HOTEL: HOTEL
    }
};