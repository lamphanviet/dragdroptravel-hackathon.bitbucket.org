/**
 * Created by lamphanviet on 1/13/15.
 */
var TRIP_COUNTER = 0;
function Trip() {
    this.id = guid();
    this.name = "SampleJourney (" + TRIP_COUNTER++ + ")";
    this.startDate = 0;
    this.duration = 0; // number of days
    this.travellers = 0; // number of travellers
    this.preferenceProfile = new PreferenceProfile(); // random one

    this.fromLocation = null; // Region
    this.destinations = []; // list of Destinations
    this.segments = [];
    this.totalPrice = 0;
}

Trip.prototype.fromJson = function(jsonString) {
    var obj = JSON.parse(jsonString);
    this.id = obj.id;
    this.duration = obj.duration;
    this.travellers = obj.travellers;
}
