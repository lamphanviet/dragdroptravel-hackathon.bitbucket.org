/**
 * Created by lamphanviet on 1/13/15.
 */
app.factory('regionService', [function() {
    var regions = [];
    var haveRegionData = false;

    function init() {
        regions = [];
        for (var i = 0; i < DataJSON.regions.length; i++) {
            regions.push(new Region(DataJSON.regions[i]));
        }
        haveRegionData = true;
        console.log("added " + regions.length + " regions");
    }

    (function() {
        init();
    })();

    function getClosetRegion(latLong) {
        var bestIndex = -1, bestDistance = 1e18;
        for (var i = 0; i < regions.length; i++) {
            if (regions[i].contain(latLong))
                return regions[i];
            var distance = latLong.disable(regions[i].latLong);
            if (distance < bestDistance) {
                bestDistance = distance;
                bestIndex = i;
            }
        }
        return regions[i];
    }

    function getRandomRegion() {
        var index = randomInRange(0, regions.length - 1);
        return regions[index];
    }

    return {
        getClosetRegion: getClosetRegion,
        getRandomRegion: getRandomRegion
    };
}]);