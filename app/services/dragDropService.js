/**
 * Created by lamphanviet on 1/13/15.
 */
app.factory('dragDropService', [function() {
    var map = null;
    var mapOptions = (function() {
        return {
            zoom: Number(localStorage.getItem("mapOption.zoom") || 8),
            center: new google.maps.LatLng(Number(localStorage.getItem("mapOption.center.lat") || -34.397),
                                           Number(localStorage.getItem("mapOption.center.lng") || 150.644)),
            mapTypeId: google.maps.MapTypeId.HYBRID
        }
    })();

    function getMap() {
        return map;
    }

    function getMapOptions() {
        return mapOptions;
    }

    function saveMapOptions() {
        localStorage.setItem("mapOption.zoom", mapOptions.zoom);
        localStorage.setItem("mapOption.center.lat", mapOptions.center.lat());
        localStorage.setItem("mapOption.center.lng", mapOptions.center.lng());
    }

    return {
        getMap: getMap,
        getMapOptions: getMapOptions,
        saveMapOptions: saveMapOptions
    };
}]);