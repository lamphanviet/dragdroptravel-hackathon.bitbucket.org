/**
 * Created by lamphanviet on 1/13/15.
 */
app.factory('tripService', ['regionService', function(regionService) {
    var trips = [];

    function computeTripSegment(trip) {
        if (!(trip instanceof Trip)) return;
        // TODO: call server api to compute this trip segments
        trip.segments = [];
        var startDate = trip.startDate;
        var prevLocation = trip.fromLocation;
        for (var i = 0; i < trip.destinations.length; i++) {
            // flight from previous location
            var flightSegment = new TripSegment({
                trip: trip,
                type: TRIP_SEGMENT_TYPE.FLIGHT,
                fromLocation: prevLocation,
                toLocation: trip.destinations[i].location,
                startDate: startDate
            });
            estimateTripSegmentPrice(flightSegment);
            trip.segments.push(flightSegment);

            // stay in hotel in this destination
            var hotelSegment = new TripSegment({
                trip: trip,
                type: TRIP_SEGMENT_TYPE.HOTEL,
                fromLocation: trip.destinations[i].location,
                toLocation: trip.destinations[i].location,
                duration: trip.destinations[i].duration,
                startDate: startDate
            });
            estimateTripSegmentPrice(hotelSegment);
            trip.segments.push(hotelSegment);

            startDate += trip.destinations[i].duration * Utils.DAYS;
            prevLocation = trip.destinations[i].location;
        }
        // flight from previous location
        var returnFlightSegment = new TripSegment({
            trip: trip,
            type: TRIP_SEGMENT_TYPE.FLIGHT,
            fromLocation: trip.destinations[trip.destinations.length - 1].location,
            toLocation: trip.fromLocation,
            startDate: startDate
        });
        estimateTripSegmentPrice(returnFlightSegment);
        trip.segments.push(returnFlightSegment);

        trip.totalPrice = 0;
        for (var i = 0; i < trip.segments.length; i++)
            trip.totalPrice += trip.segments[i].price;
    }

    var TripCounter = 0;

    function createTrip() {
        var trip = getRandomTrip();
        trip.name = "New Journey (" + TripCounter++ + ")";
        return trip;
    }

    function estimateTripSegmentPrice(segment) {
        function estimateFlightTicket(segment) {
            var distance = segment.fromLocation.latLong.distance(segment.toLocation.latLong);
            segment.price = Math.floor(distance * 0.00008); // just a random factor
        }
        function estimateHotelPrice(segment) {
            segment.price = segment.duration * 100;
        }
        if (segment.type == TRIP_SEGMENT_TYPE.FLIGHT) {
            estimateFlightTicket(segment);
        }
        else {
            estimateHotelPrice(segment);
        }
    }

    function getRandomDestination() {
        var randDst = new Destination();
        randDst.location = regionService.getRandomRegion();
        randDst.duration = randomInRange(2, 20);
        return randDst;
    }

    function getRandomTrip() {
        var randTrip = new Trip();
        randTrip.travellers = randomInRange(1, 2);
        randTrip.fromLocation = regionService.getRandomRegion();
        randTrip.duration = 30;
        randTrip.destinations = [];
        var numberOfDestinations = randomInRange(2, 4);
        for (var i = 0; i < numberOfDestinations; i++) {
            var dst = getRandomDestination();
            randTrip.destinations.push(dst);
            randTrip.duration += dst.duration;
        }
        computeTripSegment(randTrip);
        trips.push(randTrip);
        console.log("created random trip: " + JSON.stringify(randTrip));
        return randTrip;
    }

    return {
        trips: trips,
        computeTripSegment: computeTripSegment,
        getRandomTrip: getRandomTrip,
        createTrip: createTrip
    };
}]);