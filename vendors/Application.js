/**
 * Created by luong_000 on 1/14/2015.
 */
$(function(){
    $("#profile-container .btn-context").click(function() {
        var parent = $(this).parent();
        if ($(parent).hasClass("card-hide")) {
            $(parent).removeClass("card-hide");
            $(parent).addClass("card-open");
            $(this).removeClass("btn-context-right-arrow");
            $(this).addClass("btn-context-left-arrow");
        } else {
            $(parent).addClass("card-hide");
            $(parent).removeClass("card-open");
            $(this).addClass("btn-context-right-arrow");
            $(this).removeClass("btn-context-left-arrow");
        }
    });
    $("#trip-list .btn-context").click(function() {
        var parent = $(this).parent();
        if ($(parent).hasClass("card-hide")) {
            $(parent).removeClass("card-hide");
            $(parent).addClass("card-open");
            $(this).removeClass("btn-context-right-arrow");
            $(this).addClass("btn-context-left-arrow");
        } else {
            $(parent).addClass("card-hide");
            $(parent).removeClass("card-open");
            $(this).addClass("btn-context-right-arrow");
            $(this).removeClass("btn-context-left-arrow");
        }
    });
    var screenWidth = $(window).width();
    var searchBoxWidth = $("#search-container").outerWidth();
    $("#search-container").css("left", (screenWidth - searchBoxWidth) / 2);
    $("#city-list").css("width", searchBoxWidth);
    $("#city-list").css("left", (screenWidth - searchBoxWidth) / 2);
    $("#search-box").focus(function () {
        $("#city-list").addClass("show");
    });
    $("#search-box").blur(function () {
        $("#city-list").removeClass("show");
    });
//    $("#result-container .list").perfectScrollbar();
});